const brain = require('brain.js')
const data = require('./data.json')

const network = new brain.recurrent.LSTM()

let traningData = data.map(item => ({
  input: item.email,
  output: item.type
}))

network.train(traningData, {
  iterations: 200
})

console.log(
  network.run('Congratulations!! You won $10000')
)